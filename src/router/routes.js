import multiguard from 'vue-router-multiguard';
import session from 'src/assets/session'

const guard = (to, from, next) => {
  let _s = new session,
  _u = _s.loadSession()
  if(!_u) {
    next('login')
  } else {
    if(_u.user.accountStatus === 0) {
      next('login');
    }
  }
  next();
}

const reRouteIfAuthenticated = (to, from, next) => {
  let _s = new session,
  _u = _s.loadSession()
  if(_u) next('/');
  else next()
}

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: multiguard([guard]),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/zap', component: () => import('pages/zap/zap.vue') },
      { path: '/user', component: () => import('pages/user/user.vue') },
      { path: '/reports', component: () => import('pages/reports/reports.vue') }
    ]
  },

  {
    path: '/login',
    component: () => import('layouts/loginLayout.vue'),
    beforeEnter: multiguard([reRouteIfAuthenticated]),
    children: [
      { path: '/', component: () => import('pages/login/login.vue') }
    ]
  },

  {
    path: '/thankyou',
    component: () => import('layouts/loginLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/thanks/thanks.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
