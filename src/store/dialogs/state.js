export default function () {
  return {
    dialogs: {
      ZapReport: {
        open: false,
        trigger: () => {}
      },
      ZapInfo: {
        open: false,
        zap: {
          url: '',
          id: 0,
          actions: [],
          platforms: []
        },
        trigger: () => {}
      }
    }
  }
}
