// import something here
import VueMoment from 'vue-moment'
import moment from "moment";
import VueMomentJS from "vue-momentjs";
// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/cli-documentation/boot-files#Anatomy-of-a-boot-file
export default async ( { app, router, Vue  } ) => {
  Vue.use(VueMomentJS, moment);
  // Vue.use(VueMoment, {
  //   moment,
  // })
}
