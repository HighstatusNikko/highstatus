// import something here
import ReportZap from 'src/components/zap/report-zap'
import ZapInfo from 'src/components/Zapinfo/dialog'

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/cli-documentation/boot-files#Anatomy-of-a-boot-file
export default async ({ app, router, Vue  }) => {
  // something to do
  Vue.component('ReportZap', ReportZap);
  Vue.component('ZapInfo', ZapInfo);
}
