import Vue from 'vue'
import axios from 'axios'
import conf from './config'
import auth from 'src/assets/auth'
import session from 'src/assets/session'

// Vue.prototype.$axios = axios
Vue.prototype.$auth = new auth;

Vue.prototype.$axios = new axios.create({
  baseURL: conf.baseUrl,
});

if (Vue.prototype.$auth.verify()){
  Vue.prototype.$axios.defaults.headers.common['Authorization']    = Vue.prototype.$auth.load();
  Vue.prototype.$session = (new session).loadSession();
} else Vue.prototype.$authenticated = false
