import session from 'src/assets/session'

export const notify = {
  data () {
    return {
      _session: {}
    }
  },
  methods: {
		catchPostError (e) {
      if(e.response.data?.line === 671) {
        this.notify("Zap duplicate entry");
      } else {
        this.notify(e.response.data.message)
      }
    },

    notify(message) {
      this.$q.notify({
        position: 'bottom-right',
        timeout: 2500,
        textColor: 'white',
        color: 'negative',
        icon: 'warning',
        message,
      })
    }
  }
}
