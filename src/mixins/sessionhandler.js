import session from 'src/assets/session'

export const sessionHandler = {
  data () {
    return {
      _session: {}
    }
  },
  methods: {
		createSession (user) {
			this._session = new session();
			this._session.create(this.wrapUser(user));
    },
		wrapUser (user) {
			var status = 1
			if(user.status) status = user.status
			return {
				...user,
				status
			}
		}
  }
}
