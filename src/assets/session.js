import {mapGetters, mapActions} from 'vuex'

export default class session {
	loadSession () {
		try {
			return JSON.parse(atob(localStorage.getItem("_s")))
		} catch {
			return false
		}
	}
	create (user) {
		localStorage.setItem("_s", this.encodeSession({user}))
	}
	update (user) {
		try {
			localStorage.setItem("_s", this.encodeSession({user}))
			return user
		} catch {
			return false
		}
	}
	destroy() {
		localStorage.clear()
		sessionStorage.clear()
	}
	encodeSession (_s) {
		return btoa(JSON.stringify(_s))
	}
}
